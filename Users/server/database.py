import motor.motor_asyncio
from bson.objectid import ObjectId

# db connection
Mongo = 'mongodb://localhost:27017'
client = motor.motor_asyncio.AsyncIOMotorClient(Mongo)
database = client.user_db

user_collection = database.get_collection('users_collection')

#helper
def user_helper(user)-> dict:
    return {
        "id":str(user["_id"]),
        "First_name":user["First_name"],
        "Last_name": user["Last_name"],
        "Email": user["Email"]
    }


# CRUD Operations

# Retrieve all users from database
async def retrieve_users():
    users = []
    async for user in user_collection.find():
        users.append(user_helper(user))
    
    return users

# Add new User into the database
async def add_user(user_data:dict)->dict:
    user = await user_collection.insert_one(user_data)
    new_user = await user_collection.find_one({'_id':user.inserted_id})

# Retrieve User by object id or user id
async def retrieve_user(id:str) -> dict:
    user = await user_collection.find_one({"_id":ObjectId(id)})
    if user:
        return user_helper(user)

# update user data into the database using object id
async def update_user(id:str, data:dict):
    if len(data)<1:
        return False
    user = await users_collection.find_one({"_id":ObjectId(id)})

    if user:
        updated_user = await user_collection.update_one({"_id":ObjectId(id)}, {'$set':data})

        if updated_user:
            return True
        return False

# Delete a user from the database.
async def delete_user(id:str):
    user = await user_collection.find_one({"_id": ObjectId(id)})

    if user:
        await user_collection.delete_one({"_id": ObjectId(id)})
        return True