from typing import Optional 
from pydantic import BaseModel, EmailStr, Field

# add users
class UserSchema(BaseModel):
    First_name : str = Field(...)
    Last_name : str = Field(...)
    Email : EmailStr = Field(...)

    class Config:
        schema_extra = {
            "example":{
                "First_name":"John",
                "Last_name":"Doe",
                "Email":"johndoe@email.com"
            }
        }


# update users
class UpdateUserModel(BaseModel):
    First_name : Optional[str]
    Last_name : Optional[str]
    Email : Optional[EmailStr]

    class Config:
        schema_extra = {
            "example":{
                "First_name":"dhinesh",
                "Last_name":"Doe",
                "Email":"johndoe@email.com"
            }
        }

# Response Model
def ResponseModel(data, message):
    return {
        'data':[data],
        'code':200,
        'message': message
    }

# Error Response Model
def ErrorResponseModel(error, code, message):
    return {
        'Error':error,
        'code': code,
        'message': message
    }