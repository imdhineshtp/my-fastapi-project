from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from server.database import (
    add_user,
    retrieve_users,
    retrieve_user,
    update_user,
    delete_user,
)

from server.models.user_details import (
    ErrorResponseModel,
    ResponseModel,
    UserSchema,
    UpdateUserModel,
)

router = APIRouter()

#Add data
@router.post("/", response_description = "User data added into the database")
async def add_user_data(user:UserSchema = Body(...)):
    user = jsonable_encoder(user)
    new_user = await add_user(user)
    return ResponseModel(new_user, "User added Successfully")

#Retrieve data
@router.get("/", response_description= "User data retrieved")
async def get_users():
    users = await retrieve_users()
    if users:
        return ResponseModel(users, "user data retrieved")
    return ResponseModel(users, "Empty List Returned")

# Retrieve by Id
@router.get('/{id}', response_description = "User Data retrieved")
async def get_user_data(id):
    user = await retrieve_user(id)
    if user:
        return ResponseModel(user, "User data retrieved")
    return ErrorResponseModel("An Error occurred.", 404, "User is unavailable")

# Update the user 
@router.put('/{id}')
async def update_user_data(id:str, data:UpdateUserModel = Body(...)):
    data = {k: v for k, v in data.dict().items() if v is not None}
    updated_user = await(update_user(id, data))

    if updated_user:
        return ResponseModel(
            'User with Id: {} update is successfull'.format(id), "User update Successfully"
        )

    return ErrorResponseModel(
        "An Error Occurred",
        404,
        "There was a error updating the user data"
    )

# Delete a user data
@router.delete('/{id}', response_description="User data deleted")
async def delete_user_data(id:str):
    deleted_user = await delete_user(id)
    if delete_user:
        return ResponseModel(
            "User id: {} was deleted successfully".format(id), "user deleted"
        )
    return ErrorResponseModel(
        "An Error Occurred",
        404,
        "There was a error deleting the user data"
    )