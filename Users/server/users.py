from fastapi import FastAPI
from server.routes.user_details import router as UserRouter

app = FastAPI()

app.include_router(UserRouter, tags = ["User"], prefix = '/user')

# @app.get('/', tags = ['Root'])
# def read_root():
#     return {'message':'Welcome'}