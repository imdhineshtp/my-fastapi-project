from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

import requests
import json

url = 'http://127.0.0.1:8005/user/'

def add_user(fname, lname, email):
    data = {
        "First_name": fname,
        "Last_name":lname,
        "Email":email
    }
    if requests.post(url, data = json.dumps(data)):
        print('User added')
    else:
        print('There is problem in your code')

def display_user():
    response=requests.get(url).json()['data']
    for res in response:
        print('-----------------------------------')
        print('User list:\n')
        print('-----------------------------------')
        for i in range(len(res)):
            print('First Name: ', res[i].get('First_name'))
            print('Last Name: ', res[i].get('Last_name'))
            print('Email id :', res[i].get('Email'))
            print('-----------------------------------')


def sendgrid(to_email,sub,content):
    message = Mail(
                from_email=YOUR_FROM_EMAIL,
                to_emails=to_email,
                subject=sub,
                html_content=content)

    sg = SendGridAPIClient(YOUR_api_key_Here)
    response = sg.send(message)
    return response.status_code

def send_email():
    email = sendgrid(input('Enter the Email: \n'), input('Subject: \n'), input('Content: \n'))
    if email == 202:
        print('Email Send!')
    else:
        print('There is a problem with your elements')

def send_bulk_email():
    display_user()
    emails = list(map(str, input('Enter the emails with whitespace one by one in single line :\n').rstrip().split()))
    response = sendgrid(emails, input('Subject: \n'), input('Content:\n'))

    if response == 202:
        print('Bulk Email send successfully')
    else:
        print('There is a problem with your elements')

    
if __name__=='__main__':

    while(True):
        print('Hi There, Kindly enter your option:')
        print('1. Add Users\n2. Display Users\n3. Send Email\n4. Send Bulk Emails\n5. Exit')
        option= int(input('\nEnter the option: '))
        
        if option == 1:
            add_user(input('Enter the First Name:\n'), input('Enter the Last Name:\n'), input('Enter the Email id:\n'))
        
        if option == 2:
            display_user()
           
        if option == 3:
            send_email()
        
        if option == 4:
            send_bulk_email()

        if option == 5:
            break
    
