<b>Hi There, I'm Dhinesh</>

<b><h1>Project Description:</h1></b>

<p>This project was created with FAST API, SendGrid and Django web framework. </p>

<p>The Django webframework was created for render the template to make CRUD operation with GUI instead of Command Line. The Django also helps to send emails to user email that fetch from FASTAPI. "Incbuilders" is the name of django project folder, that also contains the SendGrid emailing functions. <b>If you don't like to use the GUI, download the python_file.py. It performs same as django framework</b></p>

<p>The "Users" folder is FASTAPI folder. It support all of the CRUD operations. Here I used the mongodb to store the user details. I declared only common fields like First name, Last name, and Email.</p>

<b><h2>Required modules to run this project:</h2></b>

1. Django -- <b>If you want use python_file.py, You don't need this to install</b>
2. SendGrid -- Used to send email via https://sendgrid.com API
3. Motor -- used to connect the mongodb with your API
4. FASTAPI --Build the API
5. Requests -- used to perform the CRUD operations with your API

<b><h2>Required Softwares</h2></b>
1. Python 3.6+
2. Mongo DB

<b><h2>How to Run This Project?</h2></b>
<p><b>Step 1:</b> After installed the required modules and softwares, download and extract this project. First You need to run the fastapi on your machine. Goto users folder then run the main.py file. It will deploy the api locally in your machine. It will automatically hosted in  http://localhost:8005  url. </p>
<p><b>Step 2:</b> Goto sendgrid.com, create and setup your account. Create a sendgrid api key to send the emails via the django application and copy it.</p>
<p><b>Step 3:</b> Next Open the views.py. Located the sendgrid() function. Here You need to paste your <b>sendgrid APIKEY</b> into the SendGridClient() function and paste the <b>from_email</b> that you have created and verified in sendgrid.com portal </p>
<p><b>Step 4:</b> Run Django Migration commands then run the server. It will hosted in http://localhost:8000</p>
