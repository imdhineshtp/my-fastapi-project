from django.urls import path, re_path
from . import views

urlpatterns=[
    path('', views.index,name='index'),
    path('add-user/', views.add_user, name='add-user'),
    path('send-email/', views.send_email, name='send-email'),
    path('send-email/<str:id>', views.send_email_user, name='send-email'),
    path('display-user/', views.display_user, name='display-user')

]