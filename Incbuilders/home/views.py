from django.shortcuts import render
import requests
import json

# senddgrid modules
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

# Create your views here.

def index(request):
    return render(request, 'index.html')

def add_user(request):
    if request.method == 'POST':
        first_name = request.POST.get('Firstname')
        last_name = request.POST.get('Lastname')
        Email = request.POST.get('Email_id')

        data = {
            "First_name":first_name,
            "Last_name":last_name,
            "Email":Email
        }
        url = 'http://127.0.0.1:8005/user/'
        r= requests.post(url, data = json.dumps(data))
        return render(request, 'add-user.html', {'Message':'User added '})
        
    return render(request,'add-user.html')

def sendgrid(to_email,sub,content):
    message = Mail(
                from_email=Your_SendGrid_From_EmailId,
                to_emails=to_email,
                subject=sub,
                html_content=content)

    
    sg = SendGridAPIClient(api_key)
    response = sg.send(message)

    return response.status_code

def send_email(request):
    url = 'http://127.0.0.1:8005/user/'
    response = requests.get(url)
    data = response.json()['data']
    for row in data:
        context = row

    if request.method == 'POST':
        return render(request,'send-email.html', {'Users':context, 'Code':sendgrid(request.POST.get('Email_id'),request.POST.get('Mail_sub'),request.POST.get('Email_content'))})

    return render(request, 'send-email.html', {'Users':context})

def send_email_user(request, id):
    url = 'http://127.0.0.1:8005/user/{}'.format(id)
    response = requests.get(url)
    data = response.json()['data']
    # for row in data:
    #     context = row
    if request.method == 'POST':
        return render(request,'send-email.html', {'Users':context, 'Code':sendgrid([(row['Email'])for row in data][0],request.POST.get('Mail_sub'),request.POST.get('Email_content'))})
    return render(request, 'send-email.html', {'User':[(row['Email'])for row in data][0]})
    
def display_user(request):
    url = 'http://127.0.0.1:8005/user/'
    response = requests.get(url)
    data = response.json()['data']
    for row in data:
        context = row

    
    return render(request,'display-user.html', {'Users':context})
